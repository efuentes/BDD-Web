#encoding: utf-8

require 'bddfire'

Dado(/^Que ya estoy firmado$/) do
  visit 'https://ppm.softtek.com/'

  fill_in 'USERNAME', :with => 'usuario.softtek'
  fill_in 'PASSWORD', :with => 'password.softtek'
  select 'English', :from => 'nls_language'

  click_on 'Submit'
end
    
Dado(/^Puedo ver el titulo de "(.*?)"$/) do |arg1|
  expect(page).to have_content 'Mis asignaciones'
end

Dado(/^Puedo accesar a la pagina de busqueda de solicitudes$/) do
  click_link('Search')
  click_link('Request')
end

Cuando(/^Escribo el numero de Request que busco$/) do
  fill_in 'REQUEST_ID', :with => 'numero.request'
  first(:css, '.secBtn').find('a').click
end

Entonces(/^Yo espero ver el detalle del Request seleccionado$/) do
  expect(page).to have_content 'Las consultas no generan el valor esperado'
end