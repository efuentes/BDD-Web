#encoding: utf-8

require 'bddfire'

Dado(/^que ya estoy firmado$/) do
  visit 'https://ppm.softtek.com/'

  fill_in 'USERNAME', :with => 'usuario.softtek'
  fill_in 'PASSWORD', :with => 'password.softtek'
  select 'English', :from => 'nls_language'

  click_on 'Submit'
end
    
Dado(/^puedo ver el titulo de "(.*?)"$/) do |arg1|
  expect(page).to have_content 'Mis asignaciones'
end

Dado(/^puedo accesar a la página de búsqueda de solicitudes$/) do
  click_link('Search')
  click_link('Request')
end

Cuando(/^escribo el Tipo de Request que busco$/) do
  fill_in 'REQUEST_TYPE_ID', :with => 'Defect'
  fill_in 'H_ASSIGNED_TO_USER_ID', :with => ''
end

Cuando(/^pido hacer una Búsqueda Avanzada$/) do
  click_on 'Advanced Search'
end

Cuando(/^selecciono el nombre del responsable Asignado del Request$/) do
  fill_in 'H_ASSIGNED_TO_USER_ID', :with => 'usuario.softtek'
  fill_in 'H_CREATED_BY', :with => ''
end

Cuando(/^le pido que realice la busqueda$/) do
  first(:css, '.primBtn').find('a').click
end

Entonces(/^Yo espero ver el listado de los Request asociados y seleccionar uno$/) do
  expect(page).to have_content 'Save this search as:'
  first(:css, '.normal-text a').click
end

Entonces(/^Ver el detalle del Request seleccionado$/) do
  expect(page).to have_content 'Acknowledge'
end
 