FactoryGirl.define do

=begin
  factory :dog_food_product, class: Product do
    sku         "B005DFLFKH"
    name        "Purina Dog Chow"
    description "Recommended Daily Feeding Amounts for Dogs"
    price       28.0
    association :category, factory: :cat_category
    version     0
  end

  factory :dog_category, class: Category do
    code "dogs"
    name "Dogs"
    version 0
  end

  factory :cat_category, class: Category do
    code "cats"
    name "Cats"
    version 0
  end

  factory :dog_food_category, class: Category do
    code "dog_food"
    name "Dog Food"
    association :category, factory: :dog_category
    version 0
  end
=end

end
