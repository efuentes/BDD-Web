#language: es
#encoding: utf-8

@US-002
Característica: Ver el detalle de un Request de PPM cuando se conoce su número
	Yo como Collaborador de Softtek
	Necesito necesito buscar y ver el detalle de un Request abierto asignado a mi del cual conozco su número
	De manera que pueda atenderlo

@TestLink
@BI-3
Escenario: Busqueda exitosa de Requests asignado
Dado Que ya estoy firmado
	Y Puedo ver el titulo de "Mis asignaciones"
	Y Puedo accesar a la pagina de busqueda de solicitudes
Cuando Escribo el numero de Request que busco
Entonces Yo espero ver el detalle del Request seleccionado
	
