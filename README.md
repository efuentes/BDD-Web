BDD Scenarios for Web Application
=================================

## Create a folder for BDD projects' home

On Command prompt:

            $ mkdir bdd
            $ cd bdd

## Clone base project for your BDD project's name

            $ git clone https://gitlab.com/efuentes/BDD-Web.git my-project
            $ cd my-project

## Build BDD project

            $ bundle install
            $ bundle update         

## Local run

            $ rake cucumber:run

Note: Firefox 35 has a bug with Selenium-WebDriver, if you receive error: "unable to obtain stable firefox connection in 60 seconds (127.0.0.1:7055) (Selenium::WebDriver::Error::WebDriverError)" you need to retrieve Firefox to 34.0 as downloaded from https://ftp.mozilla.org/pub/mozilla.org/firefox/releases/34.0/mac/en-US/ resolves the issue. https://code.google.com/p/selenium/issues/detail?id=8390

## Git global setup:

            $ git config --global user.name "Full name (IS)"
            $ git config --global user.email "user.name@softtek.com"

## Update README file

Update instructions to setup, clone and update tasks for teams members (other analyst/testers and development team) to use this project in the README.md file.

## Create a repository for your BDD project in the Git host

## Re-initialize Git repo for BDD project

Setup git.yml with your project's git url (like: http://ip-git-host:port/user/project-name.git)

Link local git repository to new host repository

            $ rake git:init

## Clone an already created BDD project

            $ git clone http://ip-git-host:port/user/project-name.git

## Pull latest updates from BDD project made by other team members from host

            $ rake git:pull

## Push latest updates to BDD project made by you to host

            $ rake git:push['Reason for change']